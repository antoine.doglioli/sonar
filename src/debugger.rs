

use core::fmt;
use std::io::{self , Write};
use std::str::FromStr;
use std::collections::HashMap;
use nix::libc::{user_regs_struct};
use nix::sys::wait::waitpid;
use nix::sys::ptrace::{self, AddressType};
use nix::unistd::Pid;
use iced_x86::{Decoder, DecoderOptions, Formatter, Instruction, NasmFormatter};

use crate::debugger::elf_parser::ElfInfo;
use crate::input_parser::Cmd;

pub mod elf_parser;

// For now here might be moved to better suited file
struct UserRegs(user_regs_struct);

impl fmt::Display for UserRegs {
   fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
       let user_regs_struct {
           r15,
           r14,
           r13,
           r12,
           rbp,
           rbx,
           r11,
           r10,
           r9,
           r8,
           rax,
           rcx,
           rdx,
           rsi,
           rdi,
           orig_rax,
           rip,
           cs,
           eflags,
           rsp,
           ss,
           fs_base,
           gs_base,
           ds,
           es,
           fs,
           gs } = self.0;
       write!(f, "rax: {:#018x}\trbx: {:#018x}\trcx: {:#018x}\nrdx: {:#018x}\trsi: {:#018x}\trdi: {:#018x}\nrip: {:#018x}\trsp: {:#018x}\teflags: {:#018x}", rax, rbx, rcx, rdx, rsi, rdi, rip, rsp, eflags)
   } 
}

struct Context {
    breakpoints : HashMap<usize, i64>
}

impl Context {
    fn new() -> Context {
        Context {
            breakpoints :   HashMap::new()
        }
    }

    fn add_breakpoint(&mut self, child: Pid ,addr: usize) {
        if !self.breakpoints.contains_key(&(addr as usize)) {
            let prev_instr = ptrace::read(child, addr as AddressType).expect("Failed to read memory at breakpoint address");
            self.breakpoints.insert(addr, prev_instr);
            println!("Added a breakpoint at address {:#010x}", addr);
        }
    }

    fn remove_breakpoint(&mut self, addr: usize) {
        if self.breakpoints.contains_key(&(addr as usize)) {
            self.breakpoints.remove(&addr);
            println!("Breakpoint removed at {:#010x}", addr);
        }
    }

    fn apply_breakpoints(&self, child: Pid) {
        for (addr, prev_instr) in &self.breakpoints {
            let int3 = (prev_instr & !0xff) | 0xcc;
            let addr2 = addr.clone() as AddressType;
            unsafe { ptrace::write(child, addr2, int3 as AddressType).expect("Failed to write memory at breakpoint address"); }
        }
    }

    fn restore_context(&mut self, child: Pid) {
        let mut regs = ptrace::getregs(child).expect("Failed to get child's registers");
        let prev_rip = regs.rip - 1;
        for (addr, prev_instr) in &self.breakpoints {
            let addr2 = addr.clone() as AddressType;
            let instr = prev_instr.clone() as AddressType;
            unsafe { ptrace::write(child, addr2, instr).expect("Failed to write memory at breakpoint address"); }
        }
        if self.breakpoints.contains_key(&(prev_rip as usize)) {
            regs.rip = prev_rip;
            ptrace::setregs(child, regs).expect("Failed to set child's registers");
            self.breakpoints.remove(&(prev_rip as usize));
            println!("Hit breakpoint");
            println!("rip : {:#010x}", prev_rip);
            print_instruction(child, prev_rip as usize);
        }
    }

    fn show_breakpoints(&self) {
        for (addr, _) in &self.breakpoints {
            println!("Breakpoint at {:#010x}", addr);
        }
    }
}

fn print_instruction(child: Pid, addr: usize) {
    let instr = ptrace::read(child, addr as AddressType).expect("Failed to read memory at instruction address");
    let instr = i64::to_le_bytes(instr);
    let mut decoder = Decoder::with_ip(64, &instr, addr.try_into().unwrap(), DecoderOptions::NONE);
    let instr = decoder.decode();
    let mut output = String::new();
    let mut formatter = NasmFormatter::new();
    formatter.format(&instr, &mut output);
    println!("{}", output);
}

fn cont_syscall(child : Pid) {
    ptrace::syscall(child, None).expect("Faile to use PTRACE_SYSCALL");
    let pid_status = waitpid(child, None).expect("Failed to wait");
    if pid_status == nix::sys::wait::WaitStatus::Exited(child, 0) {
        println!("Child process exited");
        std::process::exit(0);
    }
    let reg_before = ptrace::getregs(child).expect("Could not get child's registers");

    println!("Entering syscall #{}", reg_before.orig_rax);

    ptrace::syscall(child, None).expect("Failed to use PTRACE_CONT");

    let _ = waitpid(child, None).expect("Failed to wait");
    let reg_after = ptrace::getregs(child).expect("Could not get child's registers");
    println!("Syscall #{}, Result : ({}, {}), exiting with syscall : #{}", reg_before.orig_rax,
                                                reg_after.rax,
                                                reg_after.rdx,
                                                reg_after.orig_rax);
}

fn step_instruction(child: Pid, context: &mut Context) {
    context.apply_breakpoints(child);
    ptrace::step(child, None).expect("Failed to use PTRACE_STEP");
    let pid_status = waitpid(child, None).expect("Failed to wait");
    if pid_status == nix::sys::wait::WaitStatus::Exited(child, 0) {
        println!("Child process exited");
        std::process::exit(0);
    }
    context.restore_context(child);
}

fn continue_program(child: Pid, context: &mut Context) {
    context.apply_breakpoints(child);
    ptrace::cont(child, None).expect("Failed to use PTRACE_CONT");        
    let pid_status = waitpid(child, None).expect("Failed to wait");
    if pid_status == nix::sys::wait::WaitStatus::Exited(child, 0) {
        println!("Child process exited");
        std::process::exit(0);
    }
    context.restore_context(child);
}

fn print_registers(child: Pid) {
    let registers = UserRegs(ptrace::getregs(child).expect("Failed to get child's registers"));    
    //println!("rip : {:#010x}\nrax : {:#010x}\nrbx : {:#010x}\nrcx : {:#010x}\n", 
    //    registers.rip, registers.rax, registers.rbx, registers.rcx);
    println!("{registers}");
}

fn show_memory(child : Pid, addr: usize) {
    let mem_read = ptrace::read(child, addr as AddressType).expect("Failed to get child's registers");
    println!("{:#018x}", mem_read);
}

fn print_next_instruction(child : Pid) {
   let registers =  ptrace::getregs(child).expect("Failed to get child's registers");
   print_instruction(child, registers.rip as usize)
}

fn help() {
    println!("Commands:");
    println!("cont - Continue execution");
    println!("csys - Continue until next syscall");
    println!("s - Step one instruction");
    println!("regs - Show registers");
    println!("m <address> - Show memory at address");
    println!("b <address> - Set breakpoint at address");
    println!("db <address> - Delete breakpoint at address");
    println!("sb - Show breakpoints");
    println!("dis <address> - Disassemble instruction at address");
    println!("symb - Show symbols");
    println!("q - Quit");
}

pub fn run(child : Pid, path : &str) {
    let _ = waitpid(child, None).expect("Failed to wait");
    ptrace::syscall(child, None).expect("Faile to use PTRACE_SYSCALL");
    let mut context = Context::new();

    println!("Loading executable info...");
    let elf_info : ElfInfo = ElfInfo::from_file(path, child);

    loop {
        print_next_instruction(child);
        print!("sonar>");
        let mut buffer = String::new();
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut buffer).unwrap();
        let cmd = Cmd::from_str(&buffer, &elf_info);

        match cmd {
            Err(_) => {
                println!("Unknown command {}", buffer);
                continue;
            }
            Ok(cmd) => {
                match cmd {
                    Cmd::Continue => continue_program(child, &mut context),
                    Cmd::ContSyscall => cont_syscall(child),
                    Cmd::Stepi => step_instruction(child, &mut context),
                    Cmd::ShowRegisters => print_registers(child),
                    Cmd::ShowMemory { address } => show_memory(child, address),
                    Cmd::Quit => break,
                    Cmd::Breakpoint { address } => context.add_breakpoint(child, address),
                    Cmd::DeleteBreakpoint { address } => context.remove_breakpoint(address),
                    Cmd::Disassemble { address } => print_instruction(child, address),
                    Cmd::ShowBreakpoints => context.show_breakpoints(),
                    Cmd::Help => help(),
                    Cmd::ShowSymbols => elf_info.print_symbols(),
                    _ => todo!(),
                }
            }
        }
    };
}

