use std::{collections::HashMap, str::FromStr};

use crate::debugger::elf_parser::{Elf64_Sym, ElfInfo};

pub enum Cmd {
    Continue,
    ContSyscall,
    Stepi,
    ShowRegisters,
    ShowMemory { address : usize },
    Breakpoint { address : usize },
    DeleteBreakpoint { address : usize },
    ShowBreakpoints,
    Disassemble { address : usize },
    ShowSymbols,
    Help,
    Quit,
}

fn parse_address(s: &str, elf_info : &ElfInfo) -> Result<usize, ()> {
    if s.starts_with("0x") {
        let hex = s.trim_start_matches("0x");
        usize::from_str_radix(hex, 16).map_err(| _ | ())

    }
    else if elf_info.symbols.contains_key(s) {
        Ok(elf_info.symbols.get(s).expect("Impossible").st_value as usize + elf_info.load_offset as usize)
    }
    else {
        Err(())
    }
}

impl Cmd {
    pub fn from_str(s: &str, elf_info : &ElfInfo) -> Result<Self, ()> {
        match s.trim() {
            "cont" => Ok(Cmd::Continue),
            "csys" => Ok(Cmd::ContSyscall),
            "s"    => Ok(Cmd::Stepi),
            "regs" => Ok(Cmd::ShowRegisters),
            "help" => Ok(Cmd::Help),
            "q"    => Ok(Cmd::Quit),
            "sb"   => Ok(Cmd::ShowBreakpoints),
            "symb" => Ok(Cmd::ShowSymbols),
            s if s.starts_with("m ") => {
                let hex = s.trim_start_matches("m ");
                let addr = parse_address(hex, &elf_info)?;
                Ok(Cmd::ShowMemory{address : addr})
            },
            s if s.starts_with("b ") => {
                let hex = s.trim_start_matches("b ");
                let addr = parse_address(hex, &elf_info)?;
                Ok(Cmd::Breakpoint{address : addr})
            },
            s if s.starts_with("db ") => {
                let hex = s.trim_start_matches("db ");
                let addr = parse_address(hex, &elf_info)?;
                Ok(Cmd::DeleteBreakpoint{address : addr})
            },
            s if s.starts_with("dis ") => {
                let hex = s.trim_start_matches("dis ");
                let addr = parse_address(hex, &elf_info)?;
                Ok(Cmd::Disassemble{address : addr})
            },
            _      => Err(()),
        }
    }
}
