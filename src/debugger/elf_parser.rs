//#![feature(generic_const_exprs)]
//#![feature(const_evaluatable_checked)]

use std::{collections::HashMap, fs::File, hash::Hash, io::{Read, Seek, SeekFrom}};
use nix::unistd::Pid;

const EI_NIDENT : usize = 16;

type Elf64_Addr = u64;
type Elf64_Off = u64;
#[repr(C)]
pub struct Elf64_Ehdr {
    e_ident : [u8; EI_NIDENT],
    e_type : u16,
    e_machine : u16,
    e_version : u32,
    e_entry : Elf64_Addr,
    e_phoff : Elf64_Off,
    e_shoff : Elf64_Off,
    e_flags : u32,
    e_ehsize : u16,
    e_phentsize : u16,
    e_phnum : u16,
    e_shentsize : u16,
    e_shnum : u16,
    e_shstrndx : u16,
}

impl Elf64_Ehdr {
    fn from_file(file : &mut File, off : u64) -> Self {
        file.seek(SeekFrom::Start(off)).expect("Couldn't seek in the file");
        let mut buf : [u8; std::mem::size_of::<Elf64_Ehdr>()] = [0; std::mem::size_of::<Elf64_Ehdr>()];
        file.read_exact(&mut buf).expect("Couldn't read enough bytes");
        unsafe {
            std::mem::transmute::<[u8; std::mem::size_of::<Elf64_Ehdr>()], Elf64_Ehdr>(buf)
        }
    }
}

#[repr(C)]
pub struct Elf64_Phdr {
    p_type : u32,
    p_flags : u32,
    p_offset : Elf64_Off,
    p_vaddr : Elf64_Addr,
    p_paddr : Elf64_Addr,
    p_filesz : u64,
    p_memsz : u64,
    p_align : u64,
}

impl Elf64_Phdr {
    fn from_file(file : &mut File, off : u64) -> Elf64_Phdr
    {
        file.seek(SeekFrom::Start(off)).unwrap();
        let mut buf : [u8; std::mem::size_of::<Elf64_Phdr>()] = [0; std::mem::size_of::<Elf64_Phdr>()];
        file.read_exact(&mut buf).unwrap();
        unsafe {
            std::mem::transmute::<[u8; std::mem::size_of::<Elf64_Phdr>()], Elf64_Phdr>(buf)
        }
    }
}

#[derive(Clone)]
#[derive(Debug)]
#[repr(C)]
pub struct Elf64_Shdr {
    sh_name : u32,
    sh_type : u32,
    sh_flags : u64,
    sh_addr : Elf64_Addr,
    sh_offset : Elf64_Off,
    sh_size : u64,
    sh_link : u32,
    sh_info : u32,
    sh_addralign : u64,
    sh_entsize : u64,
}

impl Elf64_Shdr {
    fn from_file(file : &mut File, off : u64) -> Elf64_Shdr
    {
        file.seek(SeekFrom::Start(off)).unwrap();

        let mut buf : [u8; std::mem::size_of::<Elf64_Shdr>()] = [0; std::mem::size_of::<Elf64_Shdr>()];

        file.read_exact(&mut buf).unwrap();

        unsafe {
            std::mem::transmute::<[u8; std::mem::size_of::<Elf64_Shdr>()], Elf64_Shdr>(buf)
        }
    }
}

#[derive(Clone)]
#[repr(C)]
pub struct Elf64_Sym {
    pub st_name : u32,
    pub st_info : u8,
    pub st_other : u8,
    pub st_shndx : u16,
    pub st_value : Elf64_Addr,
    pub st_size : u64,
}

impl Elf64_Sym {
    fn from_file(file : &mut File, off : u64) -> Elf64_Sym
    {
        file.seek(SeekFrom::Start(off)).unwrap();
        let mut buf : [u8; std::mem::size_of::<Elf64_Sym>()] = [0; std::mem::size_of::<Elf64_Sym>()];
        file.read_exact(&mut buf).unwrap();
        unsafe {
            std::mem::transmute::<[u8; std::mem::size_of::<Elf64_Sym>()], Elf64_Sym>(buf)
        }
    }
}

#[repr(C)]
#[derive(Debug)]
enum ShT_Type {
    SHT_NULL	= 0,   // Null section
    SHT_PROGBITS= 1,   // Program information
    SHT_SYMTAB	= 2,   // Symbol table
    SHT_STRTAB	= 3,   // String table
    SHT_RELA	= 4,   // Relocation (w/ addend)
    SHT_NOBITS	= 8,   // Not present in file
    SHT_REL	= 9,   // Relocation (no addend)
}

// Define the type subfield of the st_info field 
// of the Elf64_Sym structure (correspond to the low halfbyte)
// source : glibc elf.h
#[repr(C)]
#[derive(Debug)]
enum ST_TYPE {
    STT_NOTYPE=0,		/* Symbol type is unspecified */
    STT_OBJECT=1,		/* Symbol is a data object */
    STT_FUNC=2	,	/* Symbol is a code object */
    STT_SECTION=3,		/* Symbol associated with a section */
    STT_FILE=4	,	/* Symbol's name is file name */
    STT_COMMON=5,		/* Symbol is a common data object */
    STT_TLS=6	,	/* Symbol is thread-local data object*/
    STT_NUM=7	,	/* Number of defined types.  */
    STT_LOOS=10	,	/* Start of OS-specific */
    STT_HIOS=12	,	/* End of OS-specific */
    STT_LOPROC=13,		/* Start of processor-specific */
    STT_HIPROC=15,		/* End of processor-specific */
}


pub struct ElfInfo {
    pub file : File,
    pub header : Elf64_Ehdr,
    pub segments : Vec<Elf64_Phdr>,
    pub sections : Vec<Elf64_Shdr>,
    pub strtab : Elf64_Shdr,
    pub shstrtab : Elf64_Shdr,
    pub symhdr : Elf64_Shdr,
    pub symbols : HashMap<String, Elf64_Sym>,
    pub load_offset : u64,
}

impl ElfInfo {
   pub fn from_file(path : &str, pid: Pid) -> ElfInfo {
        let mut file = File::open(path).expect("Couldn't open file");

        let header = Elf64_Ehdr::from_file(&mut file, 0);

        // Sections headers
        let mut sections : Vec<Elf64_Shdr> = Vec::new();
        // Sections header string table
        let mut shstrtab : Option<Elf64_Shdr> = None;
        // Sections header offset
        let mut shoff = header.e_shoff; 
        for _ in 0..header.e_shnum {
            let shdr = Elf64_Shdr::from_file(&mut file, shoff);
        
            // We assume that the last string section header is the shstrtab
            if shdr.sh_type == ShT_Type::SHT_STRTAB as u32 {
                shstrtab = Some(shdr.clone());
            }
            
            sections.push(shdr);
            shoff += std::mem::size_of::<Elf64_Shdr>() as u64;
        }
        
        // Segments headers
        let mut segments : Vec<Elf64_Phdr> = Vec::new();
        // Program header offset (segment offset)
        let mut phoff = header.e_phoff;
        for _ in 0..header.e_phnum {
            let phdr = Elf64_Phdr::from_file(&mut file, phoff);

            segments.push(phdr);

            phoff += std::mem::size_of::<Elf64_Phdr>() as u64;
        }
        
        // Symbols string table
        let mut strtab : Option<Elf64_Shdr> = None;
        // Symbol table header
        let mut symhdr : Option<Elf64_Shdr> = None;

        if let Some(shstrtab) = shstrtab.clone() {
            for shdr in &sections {
                // We check the name of each section to get pointer to particular section
                if shdr.sh_name != 0 {
                    let name = read_string_file(&mut file, shstrtab.sh_offset + shdr.sh_name as u64);

                    if name.eq(".strtab") {
                        strtab = Some(shdr.clone());
                    }

                    if name.eq(".symtab") && shdr.sh_type == ShT_Type::SHT_SYMTAB as u32 {
                        symhdr = Some(shdr.clone());
                    }
                    
                }
            }
        }

        let mut symbols : HashMap<String, Elf64_Sym> = HashMap::new();
        
        let strtab = strtab.expect("Didn't find the symbol string table");
        let shstrtab = shstrtab.expect("Didn't find the section string table");
        let symhdr = symhdr.expect("Didn't find the symbols section header");


        let nb_entry = symhdr.sh_size/(std::mem::size_of::<Elf64_Sym>() as u64);
        let mut symb_off = symhdr.sh_offset;

        for _ in 0..nb_entry {
           let shdr = Elf64_Sym::from_file(&mut file, symb_off);
            
            let name = read_string_file(&mut file, strtab.sh_offset+shdr.st_name as u64);
            
            symbols.insert(name, shdr);

            symb_off += std::mem::size_of::<Elf64_Sym>() as u64;
        }

        // We calculate the offset at wich the executable is loaded

        let load_offset = get_segment_global_offset(pid, path).expect("Couldn't get the offset at which the program is loaded");


        ElfInfo {
            file,
            header,
            segments,
            sections,
            strtab,
            shstrtab,
            symhdr,
            symbols,
            load_offset,
        }
    }
    pub fn print_symbols(&self) {
        for (name, symb) in self.symbols.iter() {
            if (symb.st_info & 0xf) == ST_TYPE::STT_FUNC as u8 {
                println!("{} at {:#010x} loaded at {:#010x}", name, symb.st_value,
                       symb.st_value+self.load_offset, );
            }
        }
    }

}

// Return the global offset at which the program is launched
fn get_segment_global_offset(pid : Pid, path : &str) -> Option<u64> {
    let maps_path = format!("/proc/{}/maps", pid);
    let mut file = File::open(maps_path).unwrap();

    let mut buf = String::new();
    file.read_to_string(&mut buf).unwrap();
    let lines = buf.lines();

    for line in lines {
        let mut elements = line.split_whitespace();
        let addrs = elements.next().unwrap(); // Addresses of begining and end of the loaded
                                              // segment in memory
        elements.next().unwrap(); // Permissions of the program, skipping
        let seg_addr = elements.next().unwrap();
        let seg_addr = u64::from_str_radix(seg_addr, 16).unwrap(); // Address in the executable of
                                                                   // the segment
        elements.next().unwrap(); // Skipping
        elements.next().unwrap(); // Skipping
        if let Some(exec_path) = elements.next() {
            // We check that the segment we're looking at correspond to one of the executable segments
            if exec_path == std::fs::canonicalize(path).unwrap().to_str().unwrap() {
                // Begining address at which the segment is really loaded in memory
                let addr_beg = u64::from_str_radix(addrs.split('-')
                                    .next().unwrap(), 16).unwrap();
                return Some(addr_beg - seg_addr);
            }
        }
    }
    None
}

fn read_string_file(file : &mut File, off: u64) -> String
{
    file.seek(SeekFrom::Start(off)).unwrap();
    let mut buf : [u8; 1] = [0; 1];
    file.read_exact(&mut buf).unwrap();
    let mut str = Vec::new();
    while buf[0] != 0u8 {
        str.push(buf[0]);
        file.read_exact(&mut buf).unwrap();
    }
    String::from_utf8(str).unwrap()
}

