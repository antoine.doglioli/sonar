

use std::ffi::{CStr, CString};

use debugger::run;
use nix::sys::ptrace;
use nix::unistd::{fork, ForkResult};

pub mod debugger;
pub mod input_parser;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage: {} <program>", args[0]);
        std::process::exit(1);
    }

    let path : &CStr = &CString::new(args[1].as_str()).unwrap();
    
    let fork_result = unsafe { fork() }.expect("Failed to fork");
    match fork_result {
        ForkResult::Parent { child } => {
           run(child, path.to_str().expect("Couldn't read the path")); 
        },
        ForkResult::Child => {
            ptrace::traceme().expect("Failed to call traceme in child");
            nix::unistd::execve::<&CStr, &CStr>(path, &[], &[]).unwrap();
        }
    }
}
