# sonar

This is a repository for a debugger in Rust.

For now the debugger can :
* Execute step by step
* Place breakpoint at label or addresses
* Continue until syscall
* Analyse the registers
* Disassemble instructions at memory location


